FROM i386/ubuntu
RUN apt-get update && apt-get install --no-install-recommends -y wget libcurl4
WORKDIR /root
RUN ln -s /home /Users
RUN wget http://www.rebol.com/downloads/v278/rebol-core-278-4-3.tar.gz
RUN tar xzvf rebol-core-278-4-3.tar.gz
ENV PATH /root/releases/rebol-core/rebol:$PATH
RUN chmod u+x /root/releases/rebol-core/rebol
RUN apt-get autoclean && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
WORKDIR /tmp
ENTRYPOINT ["/root/releases/rebol-core/rebol"]